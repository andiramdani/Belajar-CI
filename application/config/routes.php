<?php
defined('BASEPATH') OR exit('No direct script access allowed');


//routing form create news
$route['news/create'] = 'news/create';

//routing form edit news
$route['news/update/(:any)'] = 'news/update/$1';

//routing delete news
$route['news/delete/(:any)'] = 'news/delete/$1';

//routing show news
$route['news/(:any)'] = 'news/view/$1';

//routing single news
$route['news'] = 'news';

$route['default_controller'] = 'Page/view';
$route['(:any)'] = 'page/view/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
