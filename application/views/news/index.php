<h2><?php echo $title; ?></h2><hr>

<button type="submit"><a href="<?php echo site_url('news/create'); ?>"> Create News items </a></button>

<?php foreach ($news as $news_item): ?>

        <h1>
        	<a href="<?php echo site_url('news/'.$news_item['slug']); ?>">
        		<?php echo $news_item['title']; ?>
        	</a>
        </h1>

        <div class="main">
                <?php echo $news_item['text']; ?>
        </div>

        <button type="submit"><a href="<?php echo site_url('news/update/'.$news_item['id']); ?>"> Edit </a></button>

        <button type="submit"><a href="<?php echo site_url('news/delete/'.$news_item['id']); ?>"> Delete </a></button>          

<?php endforeach; ?>