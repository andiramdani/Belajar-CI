<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	 public function __construct()
        {
            parent::__construct();
            $this->load->model('news_model');
            $this->load->helper('url_helper');
        }

    // fungsi  tampilkan semua data news
    public function index()
        {
            $data['news'] = $this->news_model->get_news();
            $data['title'] = 'Arsip Berita';

            // over data
            $this->load->view('news/index' , $data);
        }

    // fungsi show / data news
    public function view($slug = NULL)
        {
             $data['news_item'] = $this->news_model->get_news($slug);

             // over data
            $this->load->view('news/view' , $data);
        }

    // fungsi create data news
	public function create()
	{
	    $this->load->helper('form');
	    $this->load->library('form_validation');

	    $data['title'] = 'Create a news item';

	    //validasi form
	    $this->form_validation->set_rules('title', '<b>Judul Masih Kosong</b>', 'required');
	    $this->form_validation->set_rules('text', '<b>Text Masih Kosong</b>', 'required');

	    if ($this->form_validation->run() === FALSE)
	    {
	      	$this->load->view('news/create' , $data);

	    }
	    else
	    {
	        $this->news_model->set_news();
	        // $this->load->view('news/success');
	        redirect('news');
	    }
	}

	// fungsi update data news
	public function update($id)
	{
	    $this->load->helper('form');
	    $this->load->library('form_validation');

	    $data['title'] = 'Create a news item';

	    //validasi form
	    $this->form_validation->set_rules('title', '<b>Judul Masih Kosong</b>', 'required');
	    $this->form_validation->set_rules('text', '<b>Text Masih Kosong</b>', 'required');

	    if ($this->form_validation->run() === FALSE)
	    {
	      	$data['news_item'] = $this->news_model->get_news_id($id);
	      	$this->load->view('news/update' , $data);

	    }
	    else
	    {
	        $this->news_model->update_news($id);
	        // $this->load->view('news/success');
	        redirect('news');
	    }
	}

	public function delete($id){
		$this->news_model->delete_news($id);
	    redirect('news');
	}
}
